//CREATE DB
var db = openDatabase("churralcool", "1.0", "Mobile storage", (5 * 1024 * 1024));

// general
var MONTHS = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

function getMonth(date) {
  return MONTHS[parseInt(date.split("-")[1]) - 1];
}

function formatDate(date) {
  return date.split("-").reverse().join("/");
}

// load method
function loadEvents() {
  db.transaction(function (tx) {
    tx.executeSql("CREATE TABLE IF NOT EXISTS events (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, title VARCHAR(100) NOT NULL, date VARCHAR(16) NOT NULL, hour VARCHAR(5), locate VARCHAR(50))", [], function (tx) {
      tx.executeSql("CREATE TABLE IF NOT EXISTS items (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, name VARCHAR(100) NOT NULL, unit VARCHAR(20))", [], function (tx) {
        tx.executeSql("CREATE TABLE IF NOT EXISTS people (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, name VARCHAR(100) NOT NULL, phone VARCHAR(15))", [], function (tx) {
          tx.executeSql("CREATE TABLE IF NOT EXISTS event_item (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, event_id INTEGER NOT NULL, item_id INTEGER NOT NULL)", [], function (tx) {
            tx.executeSql("CREATE TABLE IF NOT EXISTS event_person (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, event_id INTEGER NOT NULL, person_id INTEGER NOT NULL)", [], function (tx) {
              tx.executeSql("CREATE TABLE IF NOT EXISTS shopping (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, event_id INTEGER NOT NULL, item_id INTEGER NOT NULL, person_id INTEGER NOT NULL, count INTEGER NOT NULL, price REAL NOT NULL)", [], function (tx) {
                tx.executeSql("CREATE INDEX IF NOT EXISTS idx_event_item_event_id ON event_item (event_id)", [], function (tx) {
                  tx.executeSql("CREATE INDEX IF NOT EXISTS idx_event_item_item_id ON event_item (item_id)", [], function (tx) {
                    tx.executeSql("CREATE INDEX IF NOT EXISTS idx_event_person_event_id ON event_person (event_id)", [], function (tx) {
                      tx.executeSql("CREATE INDEX IF NOT EXISTS idx_event_person_person_id ON event_person (person_id)", [], function (tx) {
                        tx.executeSql("CREATE INDEX IF NOT EXISTS idx_shopping_event_id ON shopping (event_id)", [], function (tx) {
                          tx.executeSql("CREATE INDEX IF NOT EXISTS idx_shopping_item_id ON shopping (item_id)", [], function (tx) {
                            tx.executeSql("CREATE INDEX IF NOT EXISTS idx_shopping_person_id ON shopping (person_id)", [], function (tx) {
                              tx.executeSql("SELECT * FROM events ORDER BY date ASC", [], function (tx, results) {
                                var dataList = document.getElementById("dataList");
                                dataList.clear();
                                var count = results.rows.length;
                                if (count > 0) {
                                  var items = [],
                                      oldMes = "";
                                  for (var i = 0; i < count; i++) {
                                    var row = results.rows.item(i),
                                        item = document.createElement("div"),
                                        mes = getMonth(row.date);
                                    
                                    if (mes !== oldMes) {
                                      var header = document.createElement("div");
                                      header.setAttribute("data-bb-type", "header");
                                      header.innerHTML = mes;
                                      items.push(header);

                                      oldMes = mes;
                                    }

                                    item.setAttribute("data-bb-type", "item");
                                    item.setAttribute("data-bb-title", row.title);
                                    item.setAttribute("id", row.id);
                                    item.innerHTML = formatDate(row.date);
                                    if (row.hour) {
                                      item.innerHTML = item.innerHTML + " " + row.hour;
                                    }
                                    if (row.locate) {
                                      item.innerHTML = item.innerHTML + " - " + row.locate;
                                    }
                                    item.onclick = function () {
                                      bb.pushScreen("showEvent.html", "showEvent", dataList.selected);
                                    }

                                    items.push(item);
                                  }

                                  dataList.refresh(items);

                                  if (bb.scroller) {
                                    bb.scroller.refresh();
                                  }
                                } else {
                                  document.getElementById("eventsNotFound").style.display = "block";
                                }
                              });
                            });
                          });
                        });
                      });
                    });
                  });
                });
              });
            });
          });
        });
      });
    });
  });
}

function loadItems() {
  db.transaction(function (tx) {
    tx.executeSql("SELECT * FROM items ORDER BY name ASC", [], function (tx, results) {
      var dataList = document.getElementById("dataList");
      dataList.clear();
      var count = results.rows.length;
      if (count > 0) {
        var items = [],
            oldChar = "";
        for (var i = 0; i < count; i++) {
          var row = results.rows.item(i),
              firstChar = row.name.charAt(0),
              item = document.createElement("div");

          if (firstChar !== oldChar) {
            var header = document.createElement("div");
            header.setAttribute("data-bb-type", "header");
            header.innerHTML = firstChar;
            items.push(header);

            oldChar = firstChar;
          }

          item.setAttribute("data-bb-type", "item");
          item.setAttribute("data-bb-title", row.name);
          item.setAttribute("id", row.id);
          if (row.unit) {
            item.innerHTML = "Unit: " + row.unit;
          }
          item.onclick = function () {
            bb.pushScreen("editItem.html", "editItem", dataList.selected);
          }

          items.push(item);
        }

        dataList.refresh(items);

        if (bb.scroller) {
          bb.scroller.refresh();
        }
      } else {
        document.getElementById("itemsNotFound").style.display = "block";
      }
    });
  });
}

function loadPeople() {
  db.transaction(function (tx) {
    tx.executeSql("SELECT * FROM people ORDER BY name ASC", [], function (tx, results) {
      var dataList = document.getElementById("dataList");
      dataList.clear();
      var count = results.rows.length;
      if (count > 0) {
        var items = [],
            oldChar = "";
        for (var i = 0; i < count; i++) {
          var row = results.rows.item(i),
              firstChar = row.name.charAt(0),
              item = document.createElement("div");

          if (firstChar !== oldChar) {
            var header = document.createElement("div");
            header.setAttribute("data-bb-type", "header");
            header.innerHTML = firstChar;
            items.push(header);

            oldChar = firstChar;
          }

          item.setAttribute("data-bb-type", "item");
          item.setAttribute("data-bb-title", row.name);
          item.setAttribute("id", row.id);
          if (row.phone) {
            item.innerHTML = "Phone: " + row.phone
          }
          item.onclick = function () {
            bb.pushScreen("editPerson.html", "editPerson", dataList.selected);
          }

          items.push(item);
        }

        dataList.refresh(items);

        if (bb.scroller) {
          bb.scroller.refresh();
        }
      } else {
        document.getElementById("peopleNotFound").style.display = "block";
      }
    });
  });
}

function loadShopping(ev) {
  document.getElementById("btNewPurchase").onclick = function() {
    bb.pushScreen('newPurchase.html', 'newPurchase', ev);
  }

  document.getElementById("btNewPurchase2").onclick = function() {
    bb.pushScreen('newPurchase.html', 'newPurchase', ev);
  }

  db.transaction(function (tx) {
    tx.executeSql("SELECT sh.id, sh.count, sh.price, ev.title, it.name AS item, pe.name AS buyer FROM shopping sh INNER JOIN events ev ON sh.event_id = ev.id INNER JOIN items it ON sh.item_id = it.id INNER JOIN people pe ON sh.person_id = pe.id WHERE sh.event_id = ? ORDER BY sh.id DESC", [ev.id], function (tx, results) {
      var dataList = document.getElementById("dataList");
      dataList.clear();
      var count = results.rows.length;
      if (count > 0) {
        var items = [],
            oldChar = "",
            total = 0;
        for (var i = 0; i < count; i++) {
          var row = results.rows.item(i),
              firstChar = row.name.charAt(0),
              item = document.createElement("div");

          if (firstChar !== oldChar) {
            var header = document.createElement("div");
            header.setAttribute("data-bb-type", "header");
            header.innerHTML = firstChar;
            items.push(header);

            oldChar = firstChar;
          }

          item.setAttribute("data-bb-type", "item");
          item.setAttribute("data-bb-title", row.count + "x " + row.item);
          item.setAttribute("id", row.id);
          item.innerHTML = "Buyer: " + row.buyer + ". Price: " + row.price;
          total += row.price;
          item.onclick = function () {
            bb.pushScreen("editPurchase.html", "editPurchase", dataList.selected);
          }

          items.push(item);
        }

        document.getElementById("event").innerHTML = results.rows.item(0).title + " (" + total + ")";

        dataList.refresh(items);

        if (bb.scroller) {
          bb.scroller.refresh();
        }
      } else {
        document.getElementById("shoppingNotFound").style.display = "block";
      }
    });
  });
}

// show event
function showEvent(ev) {
  document.getElementById("event").style.display = "block";
  document.getElementById("items").style.display = "none";
  document.getElementById("people").style.display = "none";

  db.transaction(function (tx) {
    tx.executeSql("SELECT * FROM events WHERE id = ?", [ev.id], function (tx, results) {
      var evt = results.rows.item(0);
      document.getElementById("event-title").setCaption(evt.title);
      document.getElementById("date").innerHTML = formatDate(evt.date) + " " + evt.hour;
      document.getElementById("locate").innerHTML = evt.locate ? evt.locate : "---";

      document.getElementById("btShowEvent").onclick = function () {
        showEvent(evt.id);
      }

      document.getElementById("btShowItems").onclick = function () {
        showItems(evt.id);
      }

      document.getElementById("btShowPeople").onclick = function () {
        showPeople(evt.id);
      }

      // document.getElementById("btShopping").onclick = function () {
      //   bb.pushScreen("shoppingEvent.html", "shoppingEvent", evt);
      // }

      // document.getElementById("btInvoice").onclick = function () {
      //   bb.pushScreen("invoiceEvent.html", "invoiceEvent", evt); 
      // }

      document.getElementById("btEdit").onclick = function () {
        bb.pushScreen("editEvent.html", "editEvent", evt);
      }

      document.getElementById("btDelete").onclick = function () {
        removeEvent(evt, true);
      }
    });
  });
}

function showItems(ev_id) {
  document.getElementById("event").style.display = "none";
  document.getElementById("items").style.display = "block";
  document.getElementById("people").style.display = "none";

  db.transaction(function (tx) {
    tx.executeSql("SELECT it.id, it.name, it.unit FROM event_item ei INNER JOIN items it ON ei.item_id = it.id WHERE ei.event_id = ? ORDER BY it.name ASC", [ev_id], function (tx, results) {
      var dataList = document.getElementById("itemsList");
      dataList.clear();
      var count = results.rows.length;
      if (count > 0) {
        var items = [],
            oldChar = "";
        for (var i = 0; i < count; i++) {
          var row = results.rows.item(i),
              firstChar = row.name.charAt(0),
              item = document.createElement("div");

          if (firstChar !== oldChar) {
            var header = document.createElement("div");
            header.setAttribute("data-bb-type", "header");
            header.innerHTML = firstChar;
            items.push(header);

            oldChar = firstChar;
          }

          item.setAttribute("data-bb-type", "item");
          item.setAttribute("data-bb-title", row.name);
          item.setAttribute("id", row.id);
          if (row.unit) {
            item.innerHTML = "Unit: " + row.unit;
          }

          items.push(item);
        }

        dataList.refresh(items);

        if (bb.scroller) {
          bb.scroller.refresh();
        }
      } else {
        document.getElementById("itemsNotFound").style.display = "block";
      }
    });
  });
}

function showPeople(ev_id) {
  document.getElementById("event").style.display = "none";
  document.getElementById("items").style.display = "none";
  document.getElementById("people").style.display = "block";

  db.transaction(function (tx) {
    tx.executeSql("SELECT pe.id, pe.name, pe.phone FROM event_person ep INNER JOIN people pe ON ep.person_id = pe.id WHERE ep.event_id = ? ORDER BY pe.name ASC", [ev_id], function (tx, results) {
      var dataList = document.getElementById("peopleList");
      dataList.clear();
      var count = results.rows.length;
      if (count > 0) {
        var items = [],
            oldChar = "";
        for (var i = 0; i < count; i++) {
          var row = results.rows.item(i),
              firstChar = row.name.charAt(0),
              item = document.createElement("div");

          if (firstChar !== oldChar) {
            var header = document.createElement("div");
            header.setAttribute("data-bb-type", "header");
            header.innerHTML = firstChar;
            items.push(header);

            oldChar = firstChar;
          }

          item.setAttribute("data-bb-type", "item");
          item.setAttribute("data-bb-title", row.name);
          item.setAttribute("id", row.id);
          if (row.phone) {
            item.innerHTML = "Phone: " + row.phone
          }

          items.push(item);
        }

        dataList.refresh(items);

        if (bb.scroller) {
          bb.scroller.refresh();
        }
      } else {
        document.getElementById("peopleNotFound").style.display = "block";
      }
    });
  });
}

// new & edit event
function eventFormShowEvent() {
  document.getElementById("event").style.display = "block";
  document.getElementById("items").style.display = "none";
  document.getElementById("people").style.display = "none";
}

function eventFormShowItems() {
  document.getElementById("event").style.display = "none";
  document.getElementById("items").style.display = "block";
  document.getElementById("people").style.display = "none";
}

function eventFormShowPeople() {
  document.getElementById("event").style.display = "none";
  document.getElementById("items").style.display = "none";
  document.getElementById("people").style.display = "block";
}

function loadDependencies() {
  db.transaction(function (tx) {
    // load items
    tx.executeSql("SELECT * FROM items ORDER BY name ASC", [], function (tx, results) {
      var count = results.rows.length;
      if (count) {
        var table = document.getElementById("items-table");
        for(var i = 0; i < count; i++) {
          var item = results.rows.item(i),
              row = document.createElement("tr"),
              label = document.createElement("td"),
              container = document.createElement("td"),
              checkbox = document.createElement("input");

          label.className = "cell";
          label.style.paddingTop = "7px";
          label.innerHTML = item.name;

          checkbox.className = "itemsCheckbox";
          checkbox.setAttribute("type", "checkbox");
          checkbox.setAttribute("value", item.id);
          checkbox = bb.checkbox.style(checkbox);

          container.className = "cell";
          container.appendChild(checkbox);

          row.appendChild(label);
          row.appendChild(container);
          table.appendChild(row);
        }
        bb.refresh();
      } else {
        document.getElementById("itemsNotFound").style.display = "block";
      }
    });
    // load people
    tx.executeSql("SELECT * FROM people ORDER BY name ASC", [], function (tx, results) {
      var count = results.rows.length;
      if (count) {
        var table = document.getElementById("people-table");
        for(var i = 0; i < count; i++) {
          var item = results.rows.item(i),
              row = document.createElement("tr"),
              label = document.createElement("td"),
              container = document.createElement("td"),
              checkbox = document.createElement("input");

          label.className = "cell";
          label.style.paddingTop = "7px";
          label.innerHTML = item.name;

          checkbox.className = "peopleCheckbox";
          checkbox.setAttribute("type", "checkbox");
          checkbox.setAttribute("value", item.id);
          checkbox = bb.checkbox.style(checkbox);

          container.className = "cell";
          container.appendChild(checkbox);

          row.appendChild(label);
          row.appendChild(container);
          table.appendChild(row);
        }
        bb.refresh();
      } else {
        document.getElementById("peopleNotFound").style.display = "block";
      }
    });
  });
}

// edit method
function editEvent(ev) {
  db.transaction(function (tx) {
    tx.executeSql("SELECT * FROM events WHERE id = ?", [ev.id], function (tx, results) {
      var row = results.rows.item(0);
      document.getElementById("id").value = row.id;
      document.getElementById("title").value = row.title;
      document.getElementById("date").value = row.date;
      document.getElementById("hour").value = row.hour;
      document.getElementById("locate").value = row.locate;

      // load items
      tx.executeSql("SELECT it.*, ei.event_id FROM items it LEFT JOIN event_item ei ON it.id = ei.item_id AND ei.event_id = ?", [row.id], function (tx, results) {
        var count = results.rows.length;
        if (count) {
          var table = document.getElementById("items-table");
          for(var i = 0; i < count; i++) {
            var item = results.rows.item(i),
                row = document.createElement("tr"),
                label = document.createElement("td"),
                container = document.createElement("td"),
                checkbox = document.createElement("input");

            label.className = "cell";
            label.style.paddingTop = "7px";
            label.innerHTML = item.name;

            checkbox.className = "itemsCheckbox";
            checkbox.setAttribute("type", "checkbox");
            checkbox.setAttribute("value", item.id);
            if (item.event_id) {
              checkbox.checked = true;
            }
            checkbox = bb.checkbox.style(checkbox);

            container.className = "cell";
            container.appendChild(checkbox);

            row.appendChild(label);
            row.appendChild(container);
            table.appendChild(row);
          }
          bb.refresh();
        } else {
          document.getElementById("itemsNotFound").style.display = "block";
        }
      });

      // load people
      tx.executeSql("SELECT pe.*, ep.event_id FROM people pe LEFT JOIN event_person ep ON pe.id = ep.person_id AND ep.event_id = ?", [row.id], function (tx, results) {
        var count = results.rows.length;
        if (count) {
          var table = document.getElementById("people-table");
          for(var i = 0; i < count; i++) {
            var item = results.rows.item(i),
                row = document.createElement("tr"),
                label = document.createElement("td"),
                container = document.createElement("td"),
                checkbox = document.createElement("input");

            label.className = "cell";
            label.style.paddingTop = "7px";
            label.innerHTML = item.name;

            checkbox.className = "peopleCheckbox";
            checkbox.setAttribute("type", "checkbox");
            checkbox.setAttribute("value", item.id);
            if (item.event_id) {
              checkbox.checked = true;
            }
            checkbox = bb.checkbox.style(checkbox);

            container.className = "cell";
            container.appendChild(checkbox);

            row.appendChild(label);
            row.appendChild(container);
            table.appendChild(row);
          }
          bb.refresh();
        } else {
          document.getElementById("peopleNotFound").style.display = "block";
        }
      });
    });
  });
}

function editItem(item) {
  db.transaction(function (tx) {
    tx.executeSql("SELECT * FROM items WHERE id = ?", [item.id], function (tx, results) {
      var row = results.rows.item(0);
      document.getElementById("id").value = row.id;
      document.getElementById("name").value = row.name;
      document.getElementById("unit").value = row.unit;
    });
  });
}

function editPerson(person) {
  db.transaction(function (tx) {
    tx.executeSql("SELECT * FROM people WHERE id = ?", [person.id], function (tx, results) {
      var row = results.rows.item(0);
      document.getElementById("id").value = row.id;
      document.getElementById("name").value = row.name;
      document.getElementById("phone").value = row.phone;
    });
  });
}

// add event
function addEvent() {
  showLoad();

  var title = document.getElementById("title").value.trim().toCapitalize(),
      date = document.getElementById("date").value,
      hour = document.getElementById("hour").value,
      locate = document.getElementById("locate").value.trim().toCapitalize();

  if (title == "") {
    hideLoad(true);
    toast("Title required.");
    return;
  }

  if (date == "") {
    hideLoad(true);
    toast("Date required.");
    return;
  }

  db.transaction(function (tx) {
    tx.executeSql("INSERT INTO events (title, date, hour, locate) VALUES (?, ?, ?, ?)", [title, date, hour, locate], function (tx, results) {
      var event_id = results.insertId,
          items = document.getElementsByClassName("itemsCheckbox"),
          people = document.getElementsByClassName("peopleCheckbox");
      
      for(var i = 0; i < items.length; i++) {
        if (items[i].getChecked()) {
          tx.executeSql("INSERT INTO event_item (event_id, item_id) VALUES (?, ?)", [event_id, items[i].value]);
        }
      }

      for(var i = 0; i < people.length; i++) {
        if (people[i].getChecked()) {
          tx.executeSql("INSERT INTO event_person (event_id, person_id) VALUES (?, ?)", [event_id, people[i].value]);
        }
      }

      hideLoad(false);
      toast("Event added successfully.");
      bb.popScreen();
    }, function (tx, error) {
      hideLoad(true);
      alert("Ops... " + error.message);
    });  
  });
}

function addItem() {
  showLoad();

  var name = document.getElementById("name").value.trim().toCapitalize(),
      unit = document.getElementById("unit").value.trim();

  if (name == "") {
    hideLoad(true);
    toast("Name required.");
    return;
  }

  db.transaction(function (tx) {
    tx.executeSql("INSERT INTO items (name, unit) VALUES (?, ?)", [name, unit], function (tx, results) {
      hideLoad(false);
      toast("Item added successfully.");
      bb.popScreen();
    }, function (tx, error) {
      hideLoad(true);
      alert("Ops... " + error.message);
    });  
  });
}

function addPerson() {
  showLoad();

  var name = document.getElementById("name").value.trim().toCapitalize(),
      phone = document.getElementById("phone").value.trim();

  if (name == "") {
    hideLoad(true);
    toast("Name required.");
    return;
  }

  db.transaction(function (tx) {
    tx.executeSql("INSERT INTO people (name, phone) VALUES (?, ?)", [name, phone], function (tx, results) {
      hideLoad(false);
      toast("Person added successfully.");
      bb.popScreen();
    }, function (tx, error) {
      hideLoad(true);
      alert("Ops... " + error.message);
    });  
  });
}

// update method
function updateEvent() {
  showLoad();

  var id = document.getElementById("id").value,
      title = document.getElementById("title").value.trim().toCapitalize(),
      date = document.getElementById("date").value,
      hour = document.getElementById("hour").value,
      locate = document.getElementById("locate").value.trim().toCapitalize();

  if (title == "") {
    hideLoad(true);
    toast("Title required.");
    return;
  }

  if (date == "") {
    hideLoad(true);
    toast("Date required.");
    return;
  }
 
  db.transaction(function (tx) {
    tx.executeSql("UPDATE events SET title = ?, date = ?, hour = ?, locate = ? WHERE id = ?", [title, date, hour, locate, id], function () {
      var items = document.getElementsByClassName("itemsCheckbox"),
          people = document.getElementsByClassName("peopleCheckbox");
      
      tx.executeSql("DELETE FROM event_item WHERE event_id = ?", [id]);
      for(var i = 0; i < items.length; i++) {
        if (items[i].getChecked()) {
          tx.executeSql("INSERT INTO event_item (event_id, item_id) VALUES (?, ?)", [id, items[i].value]);
        }
      }

      tx.executeSql("DELETE FROM event_person WHERE event_id = ?", [id]);
      for(var i = 0; i < people.length; i++) {
        if (people[i].getChecked()) {
          tx.executeSql("INSERT INTO event_person (event_id, person_id) VALUES (?, ?)", [id, people[i].value]);
        }
      }

      hideLoad(false);
      toast("Event updated successfully."); 
      bb.popScreen();
    });
  });
}

function updateItem() {
  showLoad();

  var id = document.getElementById("id").value,
      name = document.getElementById("name").value.trim().toCapitalize(),
      unit = document.getElementById("unit").value;

  if (name == "") {
    hideLoad(true);
    toast("Name required.");
    return;
  }
 
  db.transaction(function (tx) {
    tx.executeSql("UPDATE items SET name = ?, unit = ? WHERE id = ?", [name, unit, id], function () {
      hideLoad(false);
      toast("Item updated successfully."); 
      bb.popScreen();
    });
  });
}

function updatePerson() {
  showLoad();

  var id = document.getElementById("id").value,
      name = document.getElementById("name").value.trim().toCapitalize(),
      phone = document.getElementById("phone").value;

  if (name == "") {
    hideLoad(true);
    toast("Name required.");
    return;
  }
 
  db.transaction(function (tx) {
    tx.executeSql("UPDATE people SET name = ?, phone = ? WHERE id = ?", [name, phone, id], function () {
      hideLoad(false);
      toast("Person updated successfully."); 
      bb.popScreen();
    });
  });
}

// remove method
function removeEvent(item, details) {
  var id = item.id;
  blackberry.ui.dialog.customAskAsync("This will delete the selected item", ["Cancel", "Delete"], function (idx) {
    if (idx === 1) {
      db.transaction(function (tx) {
        tx.executeSql("DELETE FROM event_item WHERE event_id = ?", [id], function (tx) {
          tx.executeSql("DELETE FROM event_person WHERE event_id = ?", [id], function (tx) {
            tx.executeSql("DELETE FROM events WHERE id = ?", [id], function (tx) {
              if (details) {
                bb.popScreen();
              } else {
                loadEvents();
              }
              toast("Event deleted successfully.");
            });
          })
        });
      });
    }
  }, {title: "Delete?"});
}

function removeItem(item) {
  var id = item.id;
  blackberry.ui.dialog.customAskAsync("This will delete the selected item and the relationship with the events", ["Cancel", "Delete"], function (idx) {
    if (idx === 1) {
      db.transaction(function (tx) {
        tx.executeSql("DELETE FROM event_item WHERE item_id = ?", [id], function (tx) {
          tx.executeSql("DELETE FROM items WHERE id = ?", [id], function (tx) {
            loadItems();
            toast("Item deleted successfully.");
          });
        })
      });
    }
  }, {title: "Delete?"});
}

function removePerson(item) {
  var id = item.id;
  blackberry.ui.dialog.customAskAsync("This will delete the selected item the relationship with the events", ["Cancel", "Delete"], function (idx) {
    if (idx === 1) {
      db.transaction(function (tx) {
        tx.executeSql("DELETE FROM event_person WHERE person_id = ?", [id], function (tx) {
          tx.executeSql("DELETE FROM people WHERE id = ?", [id], function (tx) {
            loadPeople();
            toast("Person deleted successfully.");
          });
        })
      });
    }
  }, {title: "Delete?"});
}